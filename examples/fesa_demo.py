#!/acc/local/share/python/acc-py/pro/bin/python
import sys
sys.path.append('..')

import pyjapc
import nicejapc
import pprint

# PyJAPC handle used by nicejapc
japc = pyjapc.PyJapc(incaAcceleratorName=None, noSet=True)  # NOTE! noSet enabled for safety reasons

# FESA device handle, for accessing properties
# SX.SCY-CTML is a CTIM_Key device: https://ccde.cern.ch/devices/classes/11384/version/37590/properties
ctim = nicejapc.Device(japc, 'SX.SCY-CTML')

# TODO demonstrate different accesses (mux/non-muxed, get/set/partial-set, field/property, default user)
print(ctim.Acquisition.get('SPS.USER.SFTPRO1'))             # multiplexed property get
print(ctim.Acquisition.acqC.get('SPS.USER.SFTPRO1'))        # multiplexed value-item get

print(ctim.Frame.get())                                     # non-multiplexed property get
print(ctim.Frame.frame.get())                               # non-multiplexed value-item get

print('>>> DEVICE DATA')
pprint.pprint(ctim.device_data())                                   # all kinds of device information extracted from CCDE

print('>>> CLASS DATA')
pprint.pprint(ctim.class_data())                                    # all kinds of class information extracted from CCDE

print('>>> PROPERTY DATA')
pprint.pprint(ctim.property_data())                                 # all kinds of property information extracted from CCDE
